package com.fourcatsdev.onetomanythymeleafdto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourcatsdev.onetomanythymeleafdto.modelo.Produto;
import com.fourcatsdev.onetomanythymeleafdto.repository.ProdutoRepository;

@Service
public class ProdutoService {
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public void salvarProduto(Produto produto) {
		produtoRepository.save(produto);
	};
	
	public void apagarProduto(Long id) {
		produtoRepository.delete(buscarProdutoPorId(id));
	}
	
	public List<Produto> buscarProdutos(){
		return produtoRepository.findAll();
	}
	
	public Produto buscarProdutoPorId(Long id) {
		Optional<Produto> produto = produtoRepository.findById(id);
		if (produto.isPresent()) {
			return produto.get();
		} else {
			throw new IllegalArgumentException("Produto com id : " + id + " não existe");
		}
	}
}
