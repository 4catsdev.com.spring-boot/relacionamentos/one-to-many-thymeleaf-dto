package com.fourcatsdev.onetomanythymeleafdto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourcatsdev.onetomanythymeleafdto.modelo.Categoria;
import com.fourcatsdev.onetomanythymeleafdto.repository.CategoriaRepository;


@Service
public class CategoriaService{
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public List<Categoria> buscarCategorias(){
		return categoriaRepository.findAll();
	};
	
	
	public void salvarCategoria(Categoria categoria) {
		categoriaRepository.save(categoria);
	}

}
