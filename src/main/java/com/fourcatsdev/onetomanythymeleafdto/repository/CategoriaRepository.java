package com.fourcatsdev.onetomanythymeleafdto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.onetomanythymeleafdto.modelo.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
