package com.fourcatsdev.onetomanythymeleafdto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.onetomanythymeleafdto.modelo.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
