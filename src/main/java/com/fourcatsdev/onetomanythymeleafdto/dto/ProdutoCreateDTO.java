package com.fourcatsdev.onetomanythymeleafdto.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;

public class ProdutoCreateDTO {

	@NotBlank(message = "A descrição deve ser informada")
	private String descricao;
	
	@Min(value = 0, message = "Valor do produto não deve ser negativo")
	private double valor;
	
	private CategoriaResponseDTO categoriaResponseDTO;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public CategoriaResponseDTO getCategoriaResponseDTO() {
		return categoriaResponseDTO;
	}

	public void setCategoriaResponseDTO(CategoriaResponseDTO categoriaResponseDTO) {
		this.categoriaResponseDTO = categoriaResponseDTO;
	}	

}
