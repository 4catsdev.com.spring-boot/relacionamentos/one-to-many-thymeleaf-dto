package com.fourcatsdev.onetomanythymeleafdto.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.fourcatsdev.onetomanythymeleafdto.dto.CategoriaResponseDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.ProdutoCreateDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.ProdutoResponseDTO;
import com.fourcatsdev.onetomanythymeleafdto.modelo.Categoria;
import com.fourcatsdev.onetomanythymeleafdto.modelo.Produto;

public class ProdutoMapper {
	
	public static Produto toEntity(ProdutoCreateDTO dto) {
		Produto produto = new Produto();
		produto.setValor(dto.getValor());
		produto.setDescricao(dto.getDescricao());
		Categoria categoria = CategoriaMapper.toEntity(dto.getCategoriaResponseDTO());
		produto.setCategoria(categoria);
		return produto;		
	}
	
	public static Produto toEntity(ProdutoResponseDTO dto) {
		Produto produto = new Produto();
		produto.setValor(dto.getValor());
		produto.setDescricao(dto.getDescricao());	
		produto.setId(dto.getId());
		Categoria categoria = CategoriaMapper.toEntity(dto.getCategoriaResponseDTO());
		produto.setCategoria(categoria);
		return produto;
	}
	
	public static ProdutoResponseDTO toDTO(Produto produto) {
		ProdutoResponseDTO dto = new ProdutoResponseDTO();
		dto.setId(produto.getId());
		dto.setDescricao(produto.getDescricao());
		dto.setValor(produto.getValor());
		CategoriaResponseDTO categoriaResponseDTO = CategoriaMapper.toDTO(produto.getCategoria());
		dto.setCategoriaResponseDTO(categoriaResponseDTO);
		return dto;		
	}
	
	public static List<ProdutoResponseDTO> toDTO(List<Produto> produtos) {
		return produtos.stream()
        		.map(produto -> toDTO(produto))
                .collect(Collectors.toList());
    }

}
