package com.fourcatsdev.onetomanythymeleafdto.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.fourcatsdev.onetomanythymeleafdto.dto.CategoriaCreateDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.CategoriaResponseDTO;
import com.fourcatsdev.onetomanythymeleafdto.modelo.Categoria;


public class CategoriaMapper {
	public static Categoria toEntity(CategoriaCreateDTO dto) {
		Categoria categoria = new Categoria();
		categoria.setDescricao(dto.getDescricao());				
		return categoria;		
	}
	
	public static Categoria toEntity(CategoriaResponseDTO dto) {
		Categoria categoria = new Categoria();
		categoria.setDescricao(dto.getDescricao());
		categoria.setId(dto.getId());				
		return categoria;
	}
	
	public static CategoriaResponseDTO toDTO(Categoria categoria) {
		CategoriaResponseDTO dto = new CategoriaResponseDTO();
		dto.setId(categoria.getId());
		dto.setDescricao(categoria.getDescricao());		
		return dto;		
	}
	
	public static List<CategoriaResponseDTO> toDTO(List<Categoria> categorias) {
		return categorias.stream()
        		.map(categoria -> toDTO(categoria))
                .collect(Collectors.toList());
    }

}
