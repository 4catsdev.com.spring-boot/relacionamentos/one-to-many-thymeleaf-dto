package com.fourcatsdev.onetomanythymeleafdto.dto;

import jakarta.validation.constraints.NotBlank;

public class CategoriaCreateDTO {
	
	@NotBlank(message = "A descrição deve ser informada")
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	

}
