package com.fourcatsdev.onetomanythymeleafdto.dto;

import jakarta.validation.constraints.NotBlank;

public class CategoriaResponseDTO {
	
	private Long id;
	
	@NotBlank(message = "A descrição deve ser informada")
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	

}
