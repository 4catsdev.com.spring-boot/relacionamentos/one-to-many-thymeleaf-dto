package com.fourcatsdev.onetomanythymeleafdto.controle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fourcatsdev.onetomanythymeleafdto.dto.CategoriaResponseDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.ProdutoCreateDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.ProdutoResponseDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.mapper.CategoriaMapper;
import com.fourcatsdev.onetomanythymeleafdto.dto.mapper.ProdutoMapper;
import com.fourcatsdev.onetomanythymeleafdto.modelo.Categoria;
import com.fourcatsdev.onetomanythymeleafdto.modelo.Produto;
import com.fourcatsdev.onetomanythymeleafdto.service.CategoriaService;
import com.fourcatsdev.onetomanythymeleafdto.service.ProdutoService;

import jakarta.validation.Valid;

@Controller
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	private CategoriaService categoriaService;

	@Autowired
	private ProdutoService produtoService;

	@GetMapping("/novo")
	public String adicionarProduto(Model model) {
		ProdutoCreateDTO produtoCreateDTO = new ProdutoCreateDTO();
		model.addAttribute("item", produtoCreateDTO);
		List<Categoria> categorias = categoriaService.buscarCategorias();
		List<CategoriaResponseDTO> categoriaResponseDTOs = CategoriaMapper.toDTO(categorias);
		model.addAttribute("itensCategoria", categoriaResponseDTOs);
		return "/novo-produto";
	}

	@PostMapping("/salvar")
	public String salvarProduto(@ModelAttribute("item") @Valid ProdutoCreateDTO produtoCreateDTO, BindingResult result, RedirectAttributes attributes, Model model) {
		if (produtoCreateDTO.getCategoriaResponseDTO() == null) {
			System.out.println("Chegou nulo");
		}
		if (result.hasErrors()) {			 
			List<Categoria> categorias = categoriaService.buscarCategorias();
			List<CategoriaResponseDTO> categoriaResponseDTOs = CategoriaMapper.toDTO(categorias);
			model.addAttribute("itensCategoria", categoriaResponseDTOs);
			return "/novo-produto";
		}
		Produto produto = ProdutoMapper.toEntity(produtoCreateDTO);
		produtoService.salvarProduto(produto);
		attributes.addFlashAttribute("mensagem", "Produto salvo com sucesso.");
		return "redirect:/produto/novo";
	}

	@GetMapping("/editar/{id}")
	public String editarProduto(@PathVariable Long id, Model model) {
		Produto produto = produtoService.buscarProdutoPorId(id);
		ProdutoResponseDTO produtoResponseDTO = ProdutoMapper.toDTO(produto);
		model.addAttribute("item", produtoResponseDTO);
		List<Categoria> categorias = categoriaService.buscarCategorias();
		List<CategoriaResponseDTO> categoriaResponseDTOs = CategoriaMapper.toDTO(categorias);
		model.addAttribute("itensCategoria", categoriaResponseDTOs);
		return "/alterar-produto";
	}

	@PostMapping("/editar/{id}")
	public String editarProduto(@PathVariable("id") long id, @ModelAttribute("item") @Valid ProdutoResponseDTO produtoResponseDTO, 
				BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			produtoResponseDTO.setId(id);
			List<Categoria> categorias = categoriaService.buscarCategorias();
			List<CategoriaResponseDTO> categoriaResponseDTOs = CategoriaMapper.toDTO(categorias);
			model.addAttribute("itensCategoria", categoriaResponseDTOs);
			return "/alterar-produto";
		}		
		Produto produto = ProdutoMapper.toEntity(produtoResponseDTO);
		produtoService.salvarProduto(produto);
		attributes.addFlashAttribute("mensagem", "Produto atualizado com sucesso.");
		return "redirect:/produto/editar/" + id;
	}

	@GetMapping("/apagar/{id}")
	public String apagarProduto(@PathVariable("id") long id, Model model) {
		produtoService.apagarProduto(id);
		return "redirect:/produto/listar";
	}
	
	@RequestMapping("/listar")
	public String listar(Model model) {
		List<Produto> produtos = produtoService.buscarProdutos();
		List<ProdutoResponseDTO> dtos = ProdutoMapper.toDTO(produtos);
		model.addAttribute("itens", dtos);
		return "/lista-produtos";
	}
}