package com.fourcatsdev.onetomanythymeleafdto.controle;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fourcatsdev.onetomanythymeleafdto.dto.CategoriaCreateDTO;
import com.fourcatsdev.onetomanythymeleafdto.dto.mapper.CategoriaMapper;
import com.fourcatsdev.onetomanythymeleafdto.modelo.Categoria;
import com.fourcatsdev.onetomanythymeleafdto.service.CategoriaService;

import jakarta.validation.Valid;


@Controller
@RequestMapping("/categoria")
public class CategoriaController {

	@Autowired
	private CategoriaService categoriaService;

	@GetMapping("/novo")
	public String adicionarCategoria(Model model) {
		CategoriaCreateDTO categoriaCreateDTO = new CategoriaCreateDTO();
		model.addAttribute("item", categoriaCreateDTO);
		return "/novo-categoria";
	}

	@PostMapping("/salvar")
	public String salvarCategoria(@ModelAttribute("item") @Valid CategoriaCreateDTO categoriaCreateDTO, BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return "/novo-categoria";
		}
		Categoria categoria = CategoriaMapper.toEntity(categoriaCreateDTO);
		categoriaService.salvarCategoria(categoria);
		attributes.addFlashAttribute("mensagem", "Categoria salva com sucesso.");
		return "redirect:/categoria/novo";
	}
}
